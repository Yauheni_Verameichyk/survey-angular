import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CONST_ROUTING } from './app.routing';
import { SurveyMenuComponent } from './components/survey-menu/survey-menu.component';
import { SurveyComponent } from './components/survey/survey.component';
import { UserRegisteredComponent } from './components/text/user-registered.component';
import { EqualValidator } from './services/equal-validator.directive';
import { RegistrationComponent } from './components/registration/registration.component';
import { UsersAnswersComponent } from './components/users-answers/users-answers.component';
import { SurveyPassedComponent } from './components/text/survey-passed.component';
import { PassSurveyComponent } from './components/pass-survey/pass-survey.component';
import { PassedSurveysComponent } from './components/passed-surveys/passed-surveys.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { UserComponent } from './components/user/user.component';
import { AllSurveysComponent } from './components/all-surveys/all-surveys.component';
import { SurveysComponent } from './components/surveys/surveys.component';
import { SurveysByThemeComponent } from './components/surveys-by-theme/surveys-by-theme.component';
import { LoginComponent } from './components/login/login.component';
import { ThemesComponent } from './components/themes/themes.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { AboutUsComponent } from './components/text/about-us.component';
import { OrderSurveyComponent } from './components/text/order-survey.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from './app.module';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiModule } from './api/api.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, PathLocationStrategy, LocationStrategy } from '@angular/common';
import { PaginationService } from './services/pagination.service';
import { TokenEndpointService, UserControllerService } from './api/services';
import { AuthService } from './services/auth-service';
import { RequestInterceptorService } from './services/request-interceptor.service';
import { SurveysService } from './services/surveys.service';
import { UserLoggedInGuard } from './services/user-logged-in-guard.service';
import { AnswersService } from './services/answers.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PaginationComponent,
        MainPageComponent,
        OrderSurveyComponent,
        AboutUsComponent,
        ContactsComponent,
        ThemesComponent,
        LoginComponent,
        SurveysByThemeComponent,
        SurveysComponent,
        AllSurveysComponent,
        UserComponent,
        QuestionsComponent,
        PassedSurveysComponent,
        PassSurveyComponent,
        SurveyPassedComponent,
        UsersAnswersComponent,
        RegistrationComponent,
        EqualValidator,
        UserRegisteredComponent,
        SurveyComponent,
        SurveyMenuComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        CONST_ROUTING,
        ApiModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [{provide: APP_BASE_HREF, useValue : '/' }, PaginationService, TokenEndpointService, AuthService, RequestInterceptorService, SurveysService,
      PathLocationStrategy, UserControllerService, { provide: LocationStrategy, useClass: PathLocationStrategy }, AnswersService, UserLoggedInGuard,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: RequestInterceptorService,
        multi: true
      }],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
});
