import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CONST_ROUTING } from './app.routing';
import { AppComponent } from './app.component';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_BASE_HREF, PathLocationStrategy, LocationStrategy } from '@angular/common';
import { ApiModule } from './api/api.module';
import { TokenEndpointService, UserControllerService } from './api/services';
import { PaginationComponent } from './components/pagination/pagination.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { OrderSurveyComponent } from './components/text/order-survey.component';
import { AboutUsComponent } from './components/text/about-us.component';
import { SurveysByThemeComponent } from './components/surveys-by-theme/surveys-by-theme.component';
import { ThemesComponent } from './components/themes/themes.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { LoginComponent } from './components/login/login.component';
import { SurveysComponent } from './components/surveys/surveys.component';
import { UserComponent } from './components/user/user.component';
import { AllSurveysComponent } from './components/all-surveys/all-surveys.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { PaginationService } from './services/pagination.service';
import { AuthService } from './services/auth-service';
import { RequestInterceptorService } from './services/request-interceptor.service';
import { SurveysService } from './services/surveys.service';
import { PassedSurveysComponent } from './components/passed-surveys/passed-surveys.component';
import { PassSurveyComponent } from './components/pass-survey/pass-survey.component';
import { SurveyPassedComponent } from './components/text/survey-passed.component';
import { AnswersService } from './services/answers.service';
import { UsersAnswersComponent } from './components/users-answers/users-answers.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { UserRegisteredComponent } from './components/text/user-registered.component';
import { UserLoggedInGuard } from './services/user-logged-in-guard.service';
import { SurveyComponent } from './components/survey/survey.component';
import { SurveyMenuComponent } from './components/survey-menu/survey-menu.component';
import { EqualValidator } from './services/equal-validator.directive';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    PaginationComponent,
    MainPageComponent,
    OrderSurveyComponent,
    AboutUsComponent,
    ContactsComponent,
    ThemesComponent,
    LoginComponent,
    SurveysByThemeComponent,
    SurveysComponent,
    AllSurveysComponent,
    UserComponent,
    QuestionsComponent,
    PassedSurveysComponent,
    PassSurveyComponent,
    SurveyPassedComponent,
    UsersAnswersComponent,
    RegistrationComponent,
    EqualValidator,
    UserRegisteredComponent,
    SurveyComponent,
    SurveyMenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CONST_ROUTING,
    ApiModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [PaginationService, TokenEndpointService, AuthService, RequestInterceptorService, SurveysService,
    PathLocationStrategy, UserControllerService, { provide: LocationStrategy, useClass: PathLocationStrategy }, AnswersService, UserLoggedInGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
