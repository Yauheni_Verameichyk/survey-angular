import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { convertToParamMap, ParamMap } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class ActivatedRouteStub {

  private subject = new BehaviorSubject(convertToParamMap(this.paramMap));
  params = this.subject.asObservable();

  private _testParamMap: ParamMap = convertToParamMap({});
  get paramMap() { return this._testParamMap; }
  set paramMap(params: {}) {
    this._testParamMap = convertToParamMap(params);
    this.subject.next(this._testParamMap);
  }

  get snapshot() {
    return { paramMap: this.paramMap };
  }
}
