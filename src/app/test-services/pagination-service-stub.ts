import { TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Injectable()
export class PaginationServiceStub {
  readCurrentPageNumber(ar: ActivatedRoute) {
    return +ar.snapshot.paramMap.get('currentPage');
  }

  createRange(pagesNumber: number): Array<number> {
    let items: number[] = [];
    for (let i = 1; i <= pagesNumber; i++) {
      items.push(i);
    }
    return items;
  }
}

