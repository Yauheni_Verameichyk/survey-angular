import { TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class TranslateLoaderStub implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return Observable.of("");
  }
}
