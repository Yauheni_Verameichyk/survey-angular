import { TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class SurveyServiceStub {

 public static surveys = [{
    "surveysList": [
      {
        "surveyName": "123123",
        "questionsNumber": 1
      },
      {
        "surveyName": "dbhndgk",
        "questionsNumber": 1
      },
      {
        "surveyName": "dhdrfth",
        "questionsNumber": 1
      },
      {
        "surveyName": "e2342",
        "questionsNumber": 1
      },
      {
        "surveyName": "ghdrfyj",
        "questionsNumber": 1
      }
    ],
    "currentPage": 1,
    "pagesNumber": 4
  },
  {
    "surveysList": [
      {
        "surveyName": "grdfhrt",
        "questionsNumber": 1
      },
      {
        "surveyName": "Tesg",
        "questionsNumber": 1
      },
      {
        "surveyName": "Tess",
        "questionsNumber": 1
      },
      {
        "surveyName": "Влияние соцсетей",
        "questionsNumber": 3
      },
      {
        "surveyName": "Наука и образование",
        "questionsNumber": 5
      }
    ],
    "currentPage": 2,
    "pagesNumber": 4
  }
  ]

  getAllSurveys(index: number): Observable<any> {
    return Observable.of(SurveyServiceStub.surveys[index - 1]);
  }
}

