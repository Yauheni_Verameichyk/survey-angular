import { Component, OnInit } from '@angular/core';
import { QuestionControllerService } from '../../api/services';
import { ActivatedRoute, Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { Subject } from 'rxjs';
import { Answer } from '../../models/answer';
import { JsonNode } from '../../api/models/json-node';
import { AnswersService } from '../../services/answers.service';
import { Question } from '../../models/question';

@Component({
  selector: 'app-pass-survey',
  templateUrl: './pass-survey.component.html',
  styles: []
})
export class PassSurveyComponent implements OnInit {
  surveyName: string;
  questionsList: Array<Question> = [];
  answersList: Array<Answer> = [];
  private readonly destroy: Subject<void> = new Subject();

  constructor(private questionsService: QuestionControllerService, private route: ActivatedRoute,
    private answersService: AnswersService) { }

  ngOnInit() {
    this.route.params
      .takeUntil(this.destroy)
      .subscribe(params => {
        let surveyName = this.route.snapshot.paramMap.get('surveyName');
        this.readQuestions(surveyName);
      });
  }

  readQuestions(surveyName: string): void {
    this.questionsService.getQuestionsInSurveyUsingGET_1(surveyName)
      .takeUntil(this.destroy)
      .subscribe(result => {
        this.surveyName = result['surveyName'];
        this.questionsList = result['questionsList'];
        this.answersList = this.answersService
          .fillAnswersList(new Array<Answer>(this.questionsList.length), this.questionsList);
      });
  }

  sendAnswers(): void {
    this.answersService.sendAnswersToServer(this.surveyName, this.answersList);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
