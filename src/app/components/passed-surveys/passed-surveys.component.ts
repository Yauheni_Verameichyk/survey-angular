import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurveysService } from '../../services/surveys.service';
import { PaginationService } from '../../services/pagination.service';
import { Subject } from 'rxjs';
import { Survey } from '../../models/survey';
import { Observable } from 'rxjs/Observable';
import { ObjectNode } from '../../api/models/object-node';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-passed-surveys',
  templateUrl: './passed-surveys.component.html',
  styles: []
})
export class PassedSurveysComponent implements OnInit, OnDestroy {

  passedSurveyObservable: Observable<ObjectNode>;

  // surveysList: Array<Survey> = [];
  // username: string;
  private readonly destroy: Subject<void> = new Subject();
  
  constructor(private route: ActivatedRoute, private service: SurveysService,
    private paginationService: PaginationService) { }

    ngOnInit(): void {
      this.route.params
        .takeUntil(this.destroy)
        .subscribe(params => {
          this.passedSurveyObservable = this.service.getSurveysByUser(this.paginationService.readCurrentPageNumber(this.route));
        });
    }
  
    // readSurveys(): void {
    //   this.service.getSurveysByUser(this.paginationService.readCurrentPageNumber(this.route))
    //     .takeUntil(this.destroy)
    //     .subscribe(results => {
    //       this.surveysList = results["surveysList"];
    //       this.username = results["username"];
    //       this.paginationService.sendPaginationParameters(results["pagesNumber"], results["currentPage"]);
    //     });
    // }
  
    ngOnDestroy(): void {
      this.destroy.next();
      this.destroy.complete();
    }
}
