import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ObjectNode } from '../../api/models/object-node';
import { Subject } from 'rxjs';
import { QuestionControllerService } from '../../api/services';
import { PaginationService } from '../../services/pagination.service';
import { ActivatedRoute } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html'
})
export class SurveyComponent implements OnInit, OnDestroy {

  surveyObservable: Observable<ObjectNode>;
  private readonly destroy: Subject<void> = new Subject();
  constructor(private questionsService: QuestionControllerService, private paginationService: PaginationService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params
      .takeUntil(this.destroy)
      .subscribe(params => {
        let surveyName = this.route.snapshot.paramMap.get('surveyName');
        let currentPage = this.paginationService.readCurrentPageNumber(this.route);
        this.surveyObservable = this.questionsService.getQuestionsInSurveyUsingGET({ surveyName, currentPage });
      });
  }

  ngOnDestroy():void {
    this.destroy.next();
    this.destroy.complete();
  }
}
