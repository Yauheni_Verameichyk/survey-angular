import { Component } from '@angular/core';
import { User } from '../../api/models';
import { Subject } from 'rxjs';
import { UserControllerService } from '../../api/services/user-controller.service';
import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: []
})
export class RegistrationComponent implements OnDestroy {
  user: User = new User();
  userExists: boolean = false;
  private readonly destroy: Subject<void> = new Subject();

  constructor(private userService: UserControllerService, private router: Router) { }

  registerUser(isValid: boolean): void {
    if (isValid) {
      this.userService.registrationUsingPOST(this.user)
        .subscribe(results => {
          this.checkIfUserExists(results['userExists']);
          this.checkIfUserCreated(results['userCreated']);
        })
    } else {
      console.log("Invalid form");
    }
  }

  checkIfUserExists(isUserExists: boolean): void {
    if (isUserExists) { this.userExists = true }
  }

  checkIfUserCreated(isUserCreated: boolean): void {
    if (isUserCreated) {
      this.router.navigate(['success/user']);
    }
  }

  ngOnDestroy():void {
    this.destroy.next();
    this.destroy.complete();
  }
}
