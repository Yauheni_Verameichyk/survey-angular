import { Component, Input } from '@angular/core';
import { Survey } from '../../models/survey';

@Component({
  selector: 'app-surveys',
  templateUrl: './surveys.component.html'
})
export class SurveysComponent {

  @Input()
  public surveysList: Array<Survey> = [];
  constructor() { }
}
