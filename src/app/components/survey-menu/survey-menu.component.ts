import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-survey-menu',
  templateUrl: './survey-menu.component.html'
})
export class SurveyMenuComponent {

  @Input()
  userSigned: boolean;

  @Input()
  isPassed: boolean;

  @Input()
  isAdmin: boolean;


  @Input()
  surveyName: string;
}
