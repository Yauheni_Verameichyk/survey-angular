import { TestBed, async, getTestBed } from '@angular/core/testing';
import { AllSurveysComponent } from './all-surveys.component';
import { Router } from '@angular/router';
import { SurveysComponent } from '../surveys/surveys.component';
import { PaginationComponent } from '../pagination/pagination.component';
import { Observable } from 'rxjs/Observable';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteStub } from '../../test-services/active-router-stub';
import { ActivatedRoute } from '@angular/router';
import { SurveysService } from '../../services/surveys.service';
import { PaginationService } from '../../services/pagination.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateLoaderStub } from '../../test-services/translate-loader-stub';
import { PaginationServiceStub } from '../../test-services/pagination-service-stub';
import { SurveyServiceStub } from '../../test-services/survey-service-stub';

describe('AllSurveysComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AllSurveysComponent
      ],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: TranslateLoaderStub },
        })
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: SurveysService, useClass: SurveyServiceStub },
        { provide: PaginationService, useClass: PaginationServiceStub }
      ]
    }).compileComponents();
  }));

  it('should get page content as Observable and compare it with expected', async(() => {
    const fixture = TestBed.createComponent(AllSurveysComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    app.route.paramMap = { currentPage: 2 };
    expect(app.allSurveysObservable).toEqual(Observable.of(SurveyServiceStub.surveys[1]));
  }));
});
