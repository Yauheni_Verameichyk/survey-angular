import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { PaginationService } from '../../services/pagination.service';
import { SurveysService } from '../../services/surveys.service';
import { Survey } from '../../models/survey';
import { ObjectNode } from '../../api/models/object-node';

@Component({
  selector: 'app-all-surveys',
  templateUrl: './all-surveys.component.html'
})
export class AllSurveysComponent implements OnInit, OnDestroy {

  allSurveysObservable: Observable<ObjectNode>;
  private readonly destroy: Subject<void> = new Subject();

  constructor(private route: ActivatedRoute, private service: SurveysService,
    private paginationService: PaginationService) { }

  ngOnInit(): void {
    this.route.params
      .takeUntil(this.destroy)
      .subscribe(params => {
        this.allSurveysObservable = this.service.getAllSurveys(this.paginationService.readCurrentPageNumber(this.route));
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
