import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurveysComponent } from '../surveys/surveys.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { Subject } from 'rxjs';
import { SurveysService } from '../../services/surveys.service';
import { PaginationService } from '../../services/pagination.service';
import { Survey } from '../../models/survey';
import { Observable } from 'rxjs/Observable';
import { ObjectNode } from '../../api/models/object-node';

@Component({
  selector: 'app-surveys-by-theme',
  templateUrl: './surveys-by-theme.component.html'
})
export class SurveysByThemeComponent implements OnInit, OnDestroy {

  surveysByThemeObservable: Observable<ObjectNode>;

  private readonly destroy: Subject<void> = new Subject();

  constructor(private route: ActivatedRoute, private service: SurveysService,
    private paginationService: PaginationService) { }

  ngOnInit(): void {
    this.route.params
      .takeUntil(this.destroy)
      .subscribe(params => {
        let themeName = this.service.readTheme(this.route);
        this.surveysByThemeObservable = this.service.getSurveysByTheme(themeName, this.paginationService.readCurrentPageNumber(this.route));
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
