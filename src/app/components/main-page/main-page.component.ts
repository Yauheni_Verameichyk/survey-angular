import { Component, OnInit } from '@angular/core';
import { SurveyControllerService } from '../../api/services';
import { Survey } from '../../models/survey';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html'
})
export class MainPageComponent implements OnInit {

  surveysObservable: Observable<Array<Survey>>;
  constructor(public surveyAPI: SurveyControllerService) { }

  ngOnInit(): void {
    this.surveysObservable = this.surveyAPI.showPopularSurveysUsingGET().map(result => result['surveysList']);
  }
}
