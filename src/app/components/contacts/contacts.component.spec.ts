import { TestBed, async, getTestBed } from '@angular/core/testing';
import { ContactsComponent } from './contacts.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';



let translations: any = {
  "contacts.republic.belarus": "First one",
  "contacts.adress": "Second",
  "contacts.phone": "Third",
  "contacts.email": "Forth"
};

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return Observable.of(translations);
  }
}

describe('ContactsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ContactsComponent
      ],
      imports: [
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: FakeLoader },
        })
      ]
    }).compileComponents();
  }));

  it('should create the contacts component', async(() => {
    const fixture = TestBed.createComponent(ContactsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have localizated values corresponding to tests ones`, async(() => {
    let injector = getTestBed();
    let translate = injector.get(TranslateService);
    translate.use('en');
    let fixture = TestBed.createComponent(ContactsComponent);
    fixture.detectChanges();

    let parent = fixture.debugElement.query(By.css('.text'));

    let spanIndex = 0;
    for (let i of parent.childNodes) {
      if (i.nativeNode.nodeName == 'SPAN') {
        expect(i.nativeNode.innerHTML).toEqual(Object.values(translations)[spanIndex]);
        spanIndex++
      }
    }
  }));
});
