import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { AuthService } from '../../services/auth-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {
  username: string;
  isAdmin: boolean = false;
  private readonly destroy: Subject<void> = new Subject();

  constructor(public authService: AuthService) {
  }

  ngOnInit(): void {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user != null) {
      this.username = user.username;
      this.isAdmin = user.isAdmin;
    }
  }

  login(username: string, password: string): void {
    this.authService.sendAuthorizationRequest(this.authService.generateParametersForAuthorizationRequest(username, password))
      .takeUntil(this.destroy)
      .subscribe(
      lstresult => {
        this.username = username;
        this.isAdmin = lstresult["role"] == 'ADMIN';
        this.authService.saveUserToLocalStorage(username, this.isAdmin);
        this.authService.saveTokensToLocalStorage(lstresult["access_token"], lstresult["refresh_token"]);
      });
  }

  logout(): void {
    localStorage.clear();
    this.username = null;
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
