import { TestBed, async, getTestBed, tick, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { LoginComponent } from './login.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../services/auth-service';
import { TranslateLoaderStub } from '../../test-services/translate-loader-stub';



// let translations: any = {
//   "contacts.republic.belarus": "First one",
//   "contacts.adress": "Second",
//   "contacts.phone": "Third",
//   "contacts.email": "Forth"
// };

class AuthServiceStub {
  sendAuthorizationRequest(lang: string): Observable<any> {
    return Observable.of("");
  }
}

describe('LoginComponent', () => {
  beforeEach(async(() => {
    // authService = jasmine.createSpyObj('AuthService', ['sendAuthorizationRequest',                          jasmine.createSpyObj('TranslateLoader', ['getTranslation'])
    //   'generateParametersForAuthorizationRequest', 'saveUserToLocalStorage', 'saveTokensToLocalStorage']);
    // loginComponent = new LoginComponent(authService);
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports:
      [
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: TranslateLoaderStub},
        }),
      ],
      providers: [AuthService
        // { provide: AuthService, useClass: AuthServiceStub },
      ]
    }).compileComponents();
  }));

  afterEach(() => {
    localStorage.clear();
  });

  it('should create the login component', async(() => {
    const fixture = TestBed.createComponent(LoginComponent);
    const loginComponent = fixture.debugElement.componentInstance;
    expect(loginComponent).toBeTruthy();
  }));

  it(`should have localizated values corresponding to tests ones`, fakeAsync (() => {
    const fixture = TestBed.createComponent(LoginComponent);
    const loginComponent = fixture.debugElement.componentInstance;
    console.log(loginComponent);
    console.log(loginComponent.isAdmin);
    console.log(loginComponent.username);
    console.dir(loginComponent);
    localStorage.setItem('user', JSON.stringify({ username: "qwe", isAdmin: true }));
    fixture.detectChanges();
    console.log(loginComponent);
    console.log(loginComponent.isAdmin);
    console.log(loginComponent.username);
  }));
});
