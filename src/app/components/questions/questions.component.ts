import { Component, Input } from '@angular/core';
import { Question } from '../../models/question';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styles: []
})
export class QuestionsComponent {

  @Input()
  questionsList: Array<Question> = [];
}
