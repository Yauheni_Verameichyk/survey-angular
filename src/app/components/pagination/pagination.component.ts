import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import "rxjs/add/operator/takeUntil";
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subject } from 'rxjs';
import { PaginationService } from '../../services/pagination.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent {

  @Input()
  public currentPage: number;

  @Input()
  public pagesNumber: number;
  
  constructor(public paginationService: PaginationService, private router: Router) {   }
}
