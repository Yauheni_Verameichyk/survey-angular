import { Component, OnInit } from '@angular/core';
import { User } from '../../api/models';
import { UserControllerService } from '../../api/services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
  userObservable: Observable<User>;

  constructor(private userService: UserControllerService) { }

  ngOnInit(): void {
    this.userObservable = this.userService.getUserInformationUsingGET();
  }
}
