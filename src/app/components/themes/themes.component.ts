import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { ThemesControllerService } from '../../api/services';
import { Theme } from '../../models/theme';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html'
})
export class ThemesComponent implements OnInit {
  // themesList: Array<Theme> = [];

  themesObservable: Observable< Array<Theme>>;
  private readonly destroy: Subject<void> = new Subject();
  constructor(public themesApi: ThemesControllerService) { }

  ngOnInit(): void {
    this.themesObservable = this.themesApi.getThemesListUsingGET().map(result => result['themesList']);
  }

  // getAllThemes(): void {
  //   this.themesApi.getThemesListUsingGET()
  //     .takeUntil(this.destroy)
  //     .subscribe(lstresult => {
  //       this.themesList = lstresult["themesList"];
  //     });
  // }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
