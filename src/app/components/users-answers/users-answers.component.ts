import { Component, OnInit } from '@angular/core';
import { QuestionAnswer } from '../../models/question-answer';
import { PaginationService } from '../../services/pagination.service';
import { ActivatedRoute } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import "rxjs/add/operator/takeUntil";
import { Subject } from 'rxjs';
import { AnswerControllerService } from '../../api/services';
import { Observable } from 'rxjs/Observable';
import { ObjectNode } from '../../api/models/object-node';

@Component({
  selector: 'app-users-answers',
  templateUrl: './users-answers.component.html',
  styles: []
})
export class UsersAnswersComponent implements OnInit, OnDestroy {
  
  // surveyName: string;
  // questionsAnswersList: Array<QuestionAnswer> = [];

  questionsAnswersObservable: Observable<ObjectNode>;
  private readonly destroy: Subject<void> = new Subject();

  constructor(private paginationService: PaginationService, private route: ActivatedRoute, private answersService: AnswerControllerService) { }

  ngOnInit(): void {
    this.route.params
    .takeUntil(this.destroy)
    .subscribe(params => {
      let surveyName = this.route.snapshot.paramMap.get('surveyName');
      let currentPage = this.paginationService.readCurrentPageNumber(this.route);
      this.questionsAnswersObservable =  this.answersService.getUsersAnswersUsingGET({surveyName, currentPage});
    });
  }

  // readQuestionsAndAnswers(surveyName: string, currentPage: number): void{
  //   this.answersService.getUsersAnswersUsingGET({surveyName, currentPage})
  //   .takeUntil(this.destroy)
  //   .subscribe(result => {
  //     this.surveyName = result["surveyName"];
  //     this.questionsAnswersList = result["questionsAnswersList"];
  //     this.paginationService.sendPaginationParameters(result["pagesNumber"], result["currentPage"]);
  //   });
  // }

  ngOnDestroy():void {
    this.destroy.next();
    this.destroy.complete();
  }
}
