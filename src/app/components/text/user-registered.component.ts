import { Component } from '@angular/core';

@Component({
  selector: 'app-user-registered',
  templateUrl: './text.component.html',
  styles: []
})
export class UserRegisteredComponent {
  text: string;

  constructor() { 
    this.text = 'registration.success.user.registration';
  }
}
