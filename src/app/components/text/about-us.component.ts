import { Component } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './text.component.html',
  styles: []
})
export class AboutUsComponent {
  text: string;

  constructor() { 
    this.text = 'order.survey';
  }
}
