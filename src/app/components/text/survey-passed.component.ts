import { Component } from '@angular/core';

@Component({
  selector: 'app-survey-passed',
  templateUrl: './text.component.html',
  styles: []
})
export class SurveyPassedComponent {
  text: string;

  constructor() { 
    this.text = 'survey.passed';
  }
}
