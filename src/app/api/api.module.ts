import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration } from './api-configuration';

import { AnswerControllerService } from './services/answer-controller.service';
import { UserControllerService } from './services/user-controller.service';
import { TokenEndpointService } from './services/token-endpoint.service';
import { QuestionControllerService } from './services/question-controller.service';
import { SurveyControllerService } from './services/survey-controller.service';
import { ThemesControllerService } from './services/themes-controller.service';

/**
 * Module that provides instances for all API services
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
   AnswerControllerService,
   UserControllerService,
   TokenEndpointService,
   QuestionControllerService,
   SurveyControllerService,
   ThemesControllerService
  ],
})
export class ApiModule { }
