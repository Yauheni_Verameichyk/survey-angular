/* tslint:disable */

/**
 */
export class User {
    birthDate?: string;
    email?: string;
    password?: string;
    password2?: string;
    role?: string;
    sex?: string;
    userID?: number;
    username?: string;
}
