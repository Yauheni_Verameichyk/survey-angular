/* tslint:disable */
import { View } from './view';

/**
 */
export class ModelAndView {
    empty?: boolean;
    model?: {};
    modelMap?: {[key: string]: {}};
    reference?: boolean;
    status?: string;
    view?: View;
    viewName?: string;
}
