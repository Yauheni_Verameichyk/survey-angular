/* tslint:disable */

/**
 */
export class ObjectNode {
    integralNumber?: boolean;
    array?: boolean;
    bigInteger?: boolean;
    binary?: boolean;
    boolean?: boolean;
    containerNode?: boolean;
    double?: boolean;
    float?: boolean;
    floatingPointNumber?: boolean;
    int?: boolean;
    bigDecimal?: boolean;
    long?: boolean;
    missingNode?: boolean;
    nodeType?: string;
    null?: boolean;
    number?: boolean;
    object?: boolean;
    pojo?: boolean;
    short?: boolean;
    textual?: boolean;
    valueNode?: boolean;
}
