/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse, 
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { JsonNode } from '../models/json-node';
import { ObjectNode } from '../models/object-node';


@Injectable()
export class AnswerControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param json - json
   */
  answerQuestionsUsingPOSTResponse(json: JsonNode): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = json;
    let req = new HttpRequest<any>(
      "POST",
      this.rootUrl + `/answers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: void = null;
        
        return _resp.clone({body: _body}) as HttpResponse<void>;
      })
    );
  }

  /**
   * @param json - json
   */
  answerQuestionsUsingPOST(json: JsonNode): Observable<void> {
    return this.answerQuestionsUsingPOSTResponse(json).pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param surveyName - surveyName
   * @param currentPage - currentPage
   */
  getUsersAnswersUsingGETResponse(params: AnswerControllerService.GetUsersAnswersUsingGETParams): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/answers/survey/${params.surveyName}/page/${params.currentPage}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param surveyName - surveyName
   * @param currentPage - currentPage
   */
  getUsersAnswersUsingGET(params: AnswerControllerService.GetUsersAnswersUsingGETParams): Observable<ObjectNode> {
    return this.getUsersAnswersUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }}

export module AnswerControllerService {
  export interface GetUsersAnswersUsingGETParams {
    surveyName: string;
    currentPage: number;
  }
}
