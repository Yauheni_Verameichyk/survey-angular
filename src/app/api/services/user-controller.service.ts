/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse, 
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { ObjectNode } from '../models/object-node';
import { User } from '../models/user';


@Injectable()
export class UserControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   */
  isUserSignedUsingGETResponse(): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/authentication`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   */
  isUserSignedUsingGET(): Observable<ObjectNode> {
    return this.isUserSignedUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param username - username
   * @param password - password
   */
  loginUsingPOSTResponse(params: UserControllerService.LoginUsingPOSTParams): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.username != null) __params = __params.set("username", params.username.toString());
    if (params.password != null) __params = __params.set("password", params.password.toString());
    let req = new HttpRequest<any>(
      "POST",
      this.rootUrl + `/login`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param username - username
   * @param password - password
   */
  loginUsingPOST(params: UserControllerService.LoginUsingPOSTParams): Observable<ObjectNode> {
    return this.loginUsingPOSTResponse(params).pipe(
      map(_r => _r.body)
    );
  }
  /**
   */
  logoutUsingGETResponse(): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/logout`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: void = null;
        
        return _resp.clone({body: _body}) as HttpResponse<void>;
      })
    );
  }

  /**
   */
  logoutUsingGET(): Observable<void> {
    return this.logoutUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param user - user
   */
  registrationUsingPOSTResponse(user: User): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = user;
    let req = new HttpRequest<any>(
      "POST",
      this.rootUrl + `/registration`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param user - user
   */
  registrationUsingPOST(user: User): Observable<ObjectNode> {
    return this.registrationUsingPOSTResponse(user).pipe(
      map(_r => _r.body)
    );
  }
  /**
   */
  getUserInformationUsingGETResponse(): Observable<HttpResponse<User>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: User = null;
        _body = _resp.body as User
        return _resp.clone({body: _body}) as HttpResponse<User>;
      })
    );
  }

  /**
   */
  getUserInformationUsingGET(): Observable<User> {
    return this.getUserInformationUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }}

export module UserControllerService {
  export interface LoginUsingPOSTParams {
    username: string;
    password: string;
  }
}
