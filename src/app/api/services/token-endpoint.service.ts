/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse, 
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { OAuth2AccessToken } from '../models/oauth-2access-token';


@Injectable()
export class TokenEndpointService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param parameters - parameters
   */
  getAccessTokenUsingGETResponse(parameters: any): Observable<HttpResponse<OAuth2AccessToken>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (parameters != null) __params = __params.set("parameters", parameters.toString());
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/oauth/token`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: OAuth2AccessToken = null;
        _body = _resp.body as OAuth2AccessToken
        return _resp.clone({body: _body}) as HttpResponse<OAuth2AccessToken>;
      })
    );
  }

  /**
   * @param parameters - parameters
   */
  getAccessTokenUsingGET(parameters: any): Observable<OAuth2AccessToken> {
    return this.getAccessTokenUsingGETResponse(parameters).pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param parameters - parameters
   */
  postAccessTokenUsingPOSTResponse(parameters: any): Observable<HttpResponse<OAuth2AccessToken>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __headers = __headers.set("Authorization", "Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0");
    __headers = __headers.set("Accept", "application/json");
    if (parameters != null) {
      __params = this.renderRequestParams(parameters, __params);
    }
    let req = new HttpRequest<any>(
      "POST",
      this.rootUrl + `/oauth/token`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });
      
    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: OAuth2AccessToken = null;
        _body = _resp.body as OAuth2AccessToken
        return _resp.clone({body: _body}) as HttpResponse<OAuth2AccessToken>;
      })
    );
  }

  renderRequestParams(parameters: any, __params: HttpParams): HttpParams{
    __params = __params.set("grant_type", parameters.grant_type);
    if(parameters.grant_type == "password"){
    __params = __params.set("username", parameters.username);
    __params = __params.set("password", parameters.password);
    } else {
      __params = __params.set("refresh_token", parameters.refresh_token);
    }
    return __params;
  }

  /**
   * @param parameters - parameters
   */
  postAccessTokenUsingPOST(parameters: any): Observable<OAuth2AccessToken> {
    return this.postAccessTokenUsingPOSTResponse(parameters).pipe(
      map(_r => _r.body)
    );
  }}

export module TokenEndpointService {
}
