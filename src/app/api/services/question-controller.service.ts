/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse, 
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { ObjectNode } from '../models/object-node';


@Injectable()
export class QuestionControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param surveyName - surveyName
   * @param currentPage - currentPage
   */
  getQuestionsInSurveyUsingGETResponse(params: QuestionControllerService.GetQuestionsInSurveyUsingGETParams): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/questions/survey/${params.surveyName}/page/${params.currentPage}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param surveyName - surveyName
   * @param currentPage - currentPage
   */
  getQuestionsInSurveyUsingGET(params: QuestionControllerService.GetQuestionsInSurveyUsingGETParams): Observable<ObjectNode> {
    return this.getQuestionsInSurveyUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param surveyName - surveyName
   */
  getQuestionsInSurveyUsingGET_1Response(surveyName: string): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/survey/${surveyName}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param surveyName - surveyName
   */
  getQuestionsInSurveyUsingGET_1(surveyName: string): Observable<ObjectNode> {
    return this.getQuestionsInSurveyUsingGET_1Response(surveyName).pipe(
      map(_r => _r.body)
    );
  }}

export module QuestionControllerService {
  export interface GetQuestionsInSurveyUsingGETParams {
    surveyName: string;
    currentPage: number;
  }
}
