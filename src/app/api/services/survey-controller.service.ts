/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse, 
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { ObjectNode } from '../models/object-node';


@Injectable()
export class SurveyControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param currentPage - currentPage
   */
  showAllSurveysUsingGETResponse(currentPage: number): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/surveys/page/${currentPage}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param currentPage - currentPage
   */
  showAllSurveysUsingGET(currentPage: number): Observable<ObjectNode> {
    return this.showAllSurveysUsingGETResponse(currentPage).pipe(
      map(_r => _r.body)
    );
  }
  /**
   */
  showPopularSurveysUsingGETResponse(): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/surveys/popular`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   */
  showPopularSurveysUsingGET(): Observable<ObjectNode> {
    return this.showPopularSurveysUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param themeName - themeName
   * @param currentPage - currentPage
   */
  showSurveysByThemeUsingGETResponse(params: SurveyControllerService.ShowSurveysByThemeUsingGETParams): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/surveys/theme/${params.themeName}/page/${params.currentPage}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param themeName - themeName
   * @param currentPage - currentPage
   */
  showSurveysByThemeUsingGET(params: SurveyControllerService.ShowSurveysByThemeUsingGETParams): Observable<ObjectNode> {
    return this.showSurveysByThemeUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }
  /**
   * @param currentPage - currentPage
   */
  showSurveysPassedByUserUsingGETResponse(currentPage: number): Observable<HttpResponse<ObjectNode>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    
    let req = new HttpRequest<any>(
      "GET",
      this.rootUrl + `/surveys/user/page/${currentPage}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: ObjectNode = null;
        _body = _resp.body as ObjectNode
        return _resp.clone({body: _body}) as HttpResponse<ObjectNode>;
      })
    );
  }

  /**
   * @param currentPage - currentPage
   */
  showSurveysPassedByUserUsingGET(currentPage: number): Observable<ObjectNode> {
    return this.showSurveysPassedByUserUsingGETResponse(currentPage).pipe(
      map(_r => _r.body)
    );
  }}

export module SurveyControllerService {
  export interface ShowSurveysByThemeUsingGETParams {
    themeName: string;
    currentPage: number;
  }
}
