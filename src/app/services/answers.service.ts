import { Injectable } from '@angular/core';
import { Answer } from '../models/answer';
import { JsonNode } from '../api/models';
import { AnswerControllerService } from '../api/services';
import { Router } from '@angular/router';
import { Question } from '../models/question';

@Injectable()
export class AnswersService {

  constructor(private answersService: AnswerControllerService, private router: Router) { }

  sendAnswersToServer(surveyName: string, answersList: Array<Answer>): void {
    this.answersService.answerQuestionsUsingPOST(this.generateParamsForAnswerQuestionsRequest(surveyName, answersList))
      .subscribe(() => { this.router.navigate(['success/survey']) });
  }

  removeEmptyAnswers(answersList: Array<Answer>): Array<Answer> {
    for (let i = answersList.length - 1; i >= 0; i--) {
      if (!answersList[i].answer) {
        answersList.splice(i, 1);
      }
    }
    return answersList;
  }

  generateParamsForAnswerQuestionsRequest(surveyName: string, answersList: Array<Answer>): JsonNode {
    return <JsonNode>{
      surveyName: surveyName,
      answers: this.removeEmptyAnswers(answersList)
    }
  }

  fillAnswersList(answersList: Array<Answer>, questionsList: Array<Question>): Array<Answer> {
    for (let i = 0; i < answersList.length; i++) {
      answersList[i] = new Answer();
      answersList[i].questionID = questionsList[i].questionID;
    }
    return answersList;
  }
}
