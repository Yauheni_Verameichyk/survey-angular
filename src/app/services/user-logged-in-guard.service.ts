import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import { rootRoute } from '@angular/router/src/router_module';

@Injectable()
export class UserLoggedInGuard implements CanActivate {

  constructor(public router: Router) {
  }

  canActivate() {
    if(localStorage.getItem("refresh_token")){
      return true;
    }
    else{
      this.router.navigate(['/main']);
      window.alert("You should log in to access this page!");
      return false;
    } 
  }
}
