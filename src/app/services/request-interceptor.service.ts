import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import {
  HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent,
  HttpResponse, HttpErrorResponse, HttpUserEvent, HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth-service';

@Injectable()
export class RequestInterceptorService implements HttpInterceptor {

  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private authService: AuthService) { }

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    let refresh_token = localStorage.getItem("refresh_token");
    if (refresh_token != null) {
      return req.clone({ setHeaders: { Authorization: 'Bearer ' + token } })
    } else {
      return req.clone()
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    return next.handle(this.addToken(req, this.authService.getAccessToken()))
      .catch(error => {
        console.log(error);
        if (error instanceof HttpErrorResponse) {
          switch ((<HttpErrorResponse>error).status) {
            //cruth, but I have no idea yet
            case 0:
              return this.handle401Error(req, next);
            case 400:
              return this.handle400Error(error);
            case 401:
              return this.handle401Error(req, next);
          }
        } else {
          return Observable.throw(error);
        }
      });
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    let refresh_token = localStorage.getItem("refresh_token");
    if (refresh_token) {
      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);
      let newToken = this.authService.refreshToken();
      this.tokenSubject.next(newToken);
      return next.handle(this.addToken(req, newToken));
    } else {
      return this.tokenSubject
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(this.addToken(req, token));
        });
    }
  }

  handle400Error(error) {
    return Observable.throw(error);
  }
}
