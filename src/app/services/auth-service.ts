import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { TokenEndpointService } from '../api/services/token-endpoint.service';
import { Injectable, Injector } from '@angular/core';
import { OAuth2AccessToken } from '../api/models';

@Injectable()
export class AuthService {
  constructor(private injector: Injector) { }

  getAccessToken(): string {
    return localStorage.getItem("access_token");
  }

  refreshToken(): string {
    let parameters = this.generateParametersForRefreshTokenRequest();
    localStorage.removeItem("refresh_token");
    this.sendAuthorizationRequest(parameters)
      .subscribe(
      lstresult => {
        this.saveTokensToLocalStorage(lstresult["access_token"], lstresult["refresh_token"]);
      });
    return localStorage.getItem("access_token");
  }

  sendAuthorizationRequest(parameters): Observable<OAuth2AccessToken> {
    let tokenEndpointService = this.injector.get(TokenEndpointService);
    return tokenEndpointService.postAccessTokenUsingPOST(parameters)
  }

  generateParametersForRefreshTokenRequest(): Object {
    return {
      grant_type: "refresh_token",
      refresh_token: localStorage.getItem("refresh_token")
    }
  }

  generateParametersForAuthorizationRequest(username: string, password: string): Object {
    return {
      grant_type: "password",
      username: username,
      password: password
    };
  }

  saveUserToLocalStorage(username: string, isAdmin: boolean): void {
    localStorage.setItem('user', JSON.stringify({ username, isAdmin }));
  }

  saveTokensToLocalStorage(access_token: string, refresh_token: string): void {
    localStorage.setItem('access_token', access_token);
    localStorage.setItem('refresh_token', refresh_token);
  }
}
