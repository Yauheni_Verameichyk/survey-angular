import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute, Router } from '@angular/router';
import { PathLocationStrategy } from '@angular/common';

@Injectable()
export class PaginationService {
  private paginationParameters = new Subject<any>();
  constructor(private location: PathLocationStrategy) { }

  sendPaginationParameters(pagesNumber: number, currentPage: number): void {
    this.paginationParameters.next({ pagesNumber, currentPage });
  }

  getPaginationParameters(): Observable<any> {
    return this.paginationParameters.asObservable();
  }

  readCurrentPageNumber(route: ActivatedRoute): number {
    return +route.snapshot.paramMap.get('currentPage');
  }

  createRange(pagesNumber: number): Array<number> {
    let items: number[] = [];
    for (let i = 1; i <= pagesNumber; i++) {
      items.push(i);
    }
    return items;
  }

  changePage(page, router: Router): void {
    let url = decodeURIComponent(window.location.pathname.replace(/.$/, page));
    router.navigate([url]);
  }
}
