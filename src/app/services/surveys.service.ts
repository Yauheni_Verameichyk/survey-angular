import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SurveyControllerService } from '../api/services';
import { PaginationService } from './pagination.service';

@Injectable()
export class SurveysService {
  
  constructor(private surveyAPI: SurveyControllerService, private paginationService: PaginationService) { }

  getAllSurveys(currentPage: number): Observable<Object>{
    return this.surveyAPI.showAllSurveysUsingGET(currentPage);
  }

  getSurveysByTheme(themeName: string, currentPage: number): Observable<Object>{
   return this.surveyAPI.showSurveysByThemeUsingGET({themeName, currentPage});
  }

  readTheme(route: ActivatedRoute): string {
    return route.snapshot.paramMap.get('themeName');
  }

  getSurveysByUser(currentPage: number): Observable<Object>{
    return this.surveyAPI.showSurveysPassedByUserUsingGET(currentPage);
  }
}
