import { Routes, RouterModule } from '@angular/router';
import { AllSurveysComponent } from './components/all-surveys/all-surveys.component';
import { SurveysByThemeComponent } from './components/surveys-by-theme/surveys-by-theme.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { AboutUsComponent } from './components/text/about-us.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { OrderSurveyComponent } from './components/text/order-survey.component';
import { ThemesComponent } from './components/themes/themes.component';
import { UserComponent } from './components/user/user.component';
import { PassedSurveysComponent } from './components/passed-surveys/passed-surveys.component';
import { PassSurveyComponent } from './components/pass-survey/pass-survey.component';
import { SurveyPassedComponent } from './components/text/survey-passed.component';
import { UsersAnswersComponent } from './components/users-answers/users-answers.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { UserRegisteredComponent } from './components/text/user-registered.component';
import { UserLoggedInGuard } from './services/user-logged-in-guard.service';
import { SurveyComponent } from './components/survey/survey.component';

const MAINMENU_ROUTES: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'surveys/page/:currentPage', component: AllSurveysComponent },
    { path: 'surveys/theme/:themeName/page/:currentPage', component: SurveysByThemeComponent },
    { path: 'main', component: MainPageComponent },
    { path: 'about', component: AboutUsComponent },
    { path: 'contacts', component: ContactsComponent },
    { path: 'order', component: OrderSurveyComponent },
    { path: 'themes', component: ThemesComponent },
    { path: 'user', component: UserComponent, canActivate: [UserLoggedInGuard] },
    { path: 'questions/survey/:surveyName/page/:currentPage', component: SurveyComponent },
    { path: 'surveys/user/page/:currentPage', component: PassedSurveysComponent },
    { path: 'survey/:surveyName', component: PassSurveyComponent },
    { path: 'success/survey', component: SurveyPassedComponent },
    { path: 'answers/survey/:surveyName/page/:currentPage', component: UsersAnswersComponent },
    { path: 'registration', component: RegistrationComponent },
    { path: 'success/user', component: UserRegisteredComponent }
];
export const CONST_ROUTING = RouterModule.forRoot(MAINMENU_ROUTES);
